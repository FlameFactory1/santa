import argparse
import csv
import random
import smtplib
import sys
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def send_mail(mail_host, mail_port, mail_from, mail_to, msg):
    smtp = smtplib.SMTP()
    smtp.connect(mail_host, mail_port)
    smtp.sendmail(mail_from, mail_to, msg.as_string())
    smtp.quit()

def get_message(mail_from, mail_to, person, image_file):
    msg = MIMEMultipart('alternative')
    msg["From"] = mail_from
    msg["To"] = ";".join(mail_to)
    msg['Subject'] = r"Santa The Secret"
    html = """\
    <html>
        <head><meta charset="UTF-8"></head>
        <body>
            <p>Привет, дорогой друг!</p>
            <p>Условия просты: купи подарок для человека, имя которого ты найдешь ниже, упакуй и подпиши кому. Раздача подарков состоится в январе, предположительно 11 числа. Следите за анонсами в чате.</p>
            <p>На Новый год ты даришь подарок этому человеку: """ + person + """.</p>
            """ + ("""<img src="cid:santa">""" if image_file is not None else "") + """
        </body>
    </html>
    """
    content = MIMEText(html, 'html', 'UTF-8')
    msg.attach(content)
    if image_file is not None:
        rawimg = open(image_file, 'rb')
        img = MIMEImage(rawimg.read())
        img.add_header('Content-ID', '<santa>')
        rawimg.close()
        msg.attach(img)
    return msg

def read_users():
    users = []
    reader = csv.reader(sys.stdin, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        users.append([row[0],row[1]])
    return users

def shuffle(users):
    random.shuffle(users)
    #assign to i-th email i-th+1 name
    l = []
    for i in range(len(users)):
        l.append([users[i][0], users[i-1][1]])
    return l

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--image', help='image to be inserted in mail', type=str, default=None)
    parser.add_argument('--mail_host', help='host used for mail', type=str, default="127.0.0.1")
    parser.add_argument('--mail_port', help='port used for mail', type=str, default="25")
    parser.add_argument('--mail_from', help='senders email', type=str, default="secretsanta@gmail.com")
    args = parser.parse_args()
    users = read_users()
    shuffled = shuffle(users)
    for i in shuffled:
        msg = get_message(args.mail_from, [i[0]], i[1], args.image)
        send_mail(args.mail_host, args.mail_port, args.mail_from, [i[0]], msg)
        print("sent")
    print('bye')
